import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ox {
	private char currentPlayer;
	char[][] board;

	public ox() {
        board = new char[4][4];
        currentPlayer = 'x';
        startBoard();
    }
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
    	ox g = new ox();
    	g.startBoard();
    	g.showWelcome();
    	do {
    		
    		g.showTable();
    		int row,col;
    		do {
    			System.out.print("Player " + g.currentPlayer + " Input Row,Col: ");
    			row = kb.nextInt();
    			col = kb.nextInt();
    		}
    		while (!g.pushInput(row, col));
    		g.switchPlayer();
    	}
    	while(!g.checkAllWin() && !g.fullBoard());
    	if(g.fullBoard() && !g.checkAllWin()) {
    		g.showTable();
    		System.out.println();
    		System.out.println("Draw!");
    	}else {
    		g.showTable();
    		g.switchPlayer();
    		System.out.println(Character.toUpperCase(g.currentPlayer) + " Win!");
    	}
    }
	
		
			
	
	public void showWelcome() {
		System.out.println("Welcome to OX game.");
		System.out.println("--------------------------------");
	}
	public void startBoard() {
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                board[i][j] = '-';
            }
        }
    }	
	public void switchPlayer() {
    	if (currentPlayer == 'x') {
    		currentPlayer = 'o';
    	}else {
    		currentPlayer = 'x';
    	}
    }	
	public boolean pushInput (int row, int col) {
    	if ((row >= 1) && (row < 4)) {
    		 if ((col >= 1) && (col < 4)) {
    			 if (board[row][col] == '-') {
    				 board[row][col] = currentPlayer;
    				 return true;
    			 }
    		 }
    	}
    	return false;
    }	
	

	
	private boolean checkRowCol(char c1, char c2, char c3) {
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}
	
	public boolean checkRowWin() {
		for (int i = 1; i < 4; i++) {
    		if (checkRowCol(board[i][1], board[i][2], board[i][3]) == true) {
    			return true;
    		}
    	}
    	return false;
	}
	public boolean checkColumnWin() {
		for (int i = 1; i < 4; i++) {
    		if (checkRowCol(board[1][i], board[2][i], board[3][i]) == true) {
    			 return true;
    		}
    	}
    	return false;
	}
	public boolean checkDiaWin() {
		return ((checkRowCol(board[1][1], board[2][2], board[3][3]) == true) || (checkRowCol(board[1][3], 
				board[2][2], board[3][1]) == true));
	}	
	public boolean fullBoard() {
        boolean isFull = true;
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                if (board[i][j] == '-') {
                    isFull = false;
                }
            }
        }
        return isFull;
    }
	public boolean checkAllWin() {
    	return (checkRowWin() || checkColumnWin() || checkDiaWin());
    }	
			

		
	 public void showTable() {
	    	System.out.println("-------------");
	    	for (int i = 1; i < 4; i++) {
	    		System.out.print("| ");
	    		for (int j = 1; j < 4; j++) {
	    			System.out.print(board[i][j] + " | ");
	    	    }
	    		System.out.println();
	    		System.out.println("-------------");
	    	 }
	    }
	
	}
